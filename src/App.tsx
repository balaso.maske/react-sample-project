import React from 'react';
import { dataArray } from './utils/model';
import { Section } from './components/Section';

function App() {
  return (
    <div className="App">
      <div className="container">
        <div className="row">
        <div className="col-xs-4 col-md-4">
              <Section config={dataArray} name="sdf"/>
        </div>
        </div>
      </div>
    </div>
  );
}

export default App;
