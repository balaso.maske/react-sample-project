import React, { Component } from "react";
import { InputConfig } from '../../utils/model';

interface Props {
    config: InputConfig;
    value : string,
    onChange: any,
  }

 export class Input extends Component<Props, {}>{
     textInput : any
    constructor(props: any) {
       super(props);
       this.state = {};
       this.onChangeInput = this.onChangeInput.bind(this)
       this.textInput = React.createRef();
      }

    init (){
        var key : string = this.props.config.textId || "";
        this.props.onChange(key, "", "Employee");
    }

    componentDidMount(){
        this.init()
    }

    render(){
        const type = this.props.config.type || "text"
        return(
            <div className="form-group">
                <label>{this.props.config.label}</label>
                <input type={type} ref={this.textInput}  className="form-control"
                name={this.props.config.textId} id={this.props.config.textId} value={this.props.config.value}
                onChange={this.onChangeInput}/>
            </div>
        )
    }
    onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        this.props.onChange(name, value, "Employee");
    };
}


