import React, { Component } from "react";
import { ButtonConfig } from '../../utils/model';
import { render } from '@testing-library/react';

interface Props {
    config: ButtonConfig;
    onSave : any
  }
  
  export class Button extends Component<Props, {}>{
    render(){
        return(
            <div className="form-group">
               <button className="btn btn-default" onClick={this.props.onSave}> {this.props.config.displayText} </button>
            </div>
        )
    }
  }