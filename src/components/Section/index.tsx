import React, { Component } from 'react';
import { ButtonConfig, InputConfig, Employee } from '../../utils/model';
import { Input } from '../input';
import { Button } from '../button/index';
import { Car } from './car';

interface Props {
    config: Array<any>,
    name : string,
   
}

export class Section extends Component<Props, {}>{
    state = { 
        initial: 'state',
        data : []
    }

    constructor(props: any) {
        super(props)
        this.onChange = this.onChange.bind(this)
        this.updateState = this.updateState.bind(this)
        this.onSave = this.onSave.bind(this)
    }
    init(){
        this.setState({["firstName"]: ""})
    }
    componentDidMount(){
        //this.init()
    }
    
    onChange(fieldName: string, value: string, parent? : string){
        this.updateState(fieldName, value, parent)
    }  

    updateState(fieldName: string, value: string, parent? : string) {
        console.log("Parent  -> ", parent)
        /*if(parent){
            let object : any = {}
            object[fieldName] = value;
            console.log("  object -->", this.state)
            this.setState({ [parent]: object });
        }else{
            this.setState({ [fieldName]: value });
        }*/
        this.setState({ [fieldName]: value });
    }
    onSave(){
   //  let object : any = {"firstName" : this.state.firstName,"lastName" : this.state.lastName}
   //debugger     
   //console.log(this.state.firstName ? "sd" : "sd" )
    }
    render(){
        return(
            <div>
                 {JSON.stringify(this.state)}<br /><br />
                { this.props.config.map((obj, i) =>
                    this.renderSwitch(obj)
                )}
            </div>
            
        )
    }
    renderSwitch(obj : any){
        switch(obj.objectId){
            case "input" :
                return (<Input config={obj as InputConfig} value={obj.label} onChange={this.onChange}/>);
            case "button" :
                    return (<Button config={obj as ButtonConfig}  onSave={this.onSave}/>);
            default:
        }
    }
}
