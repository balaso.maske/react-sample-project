import { Interface } from "readline"

export enum DataObject {
    template = "template",
    toggle = "toggle",
    screen = "screen",
    section = "section",
    input = "input",
    cardnumber = "cardnumber",
    button = "button",
    cvv = "cvv",
    expiry = "expiry",
    text = "text",
    condition = "condition",
    valuespec = "valuespec",
    dropdown = "dropdown",
    popularoptions = "popularoptions",
    list = "list",
    pay_option = "pay_option",
    image = "image",
    table = "table",
    emi_option = "emi_option",
    vpa = "vpa"
  }

  export interface InputConfig{
    textId: string,
    objectId : string
    placeholder?: string
    label?: string
    type?: string
    value? : string
    required?: string
    regex?: string
    maxlength?: string
    minlength?: string
    errorMsg?: string
  }

  export interface ButtonConfig{
    objectId : string
    id: string,
    displayText : string
  }

  export const buttonConfigs: ButtonConfig[] =[
    {"objectId" : "button",
      id : "1",
      "displayText" : "Submit"
    }
  ]
/*
  export const dataArray : Array<any> = [
      {
        "objectId" : "input",
        id : "firstName",
        label: "First Name",
        type : "text"
      },
      {"objectId" : "input",
        label: "password",
        type : "password"
      },
      {"objectId" : "input",
        label: "checkbox",
        type : "checkbox"
      },
      {"objectId" : "input",
        label: "Radio",
        type : "radio"
      }, {"objectId" : "button",
      id : "1",
      "displayText" : "Submit"
    }
    ];
*/
export const dataArray : Array<any> = [
  {
    "objectId": "input",
    "textId": "firstName",
    "label": "First Name",
    "type": "text"
  },
  {
    "objectId": "input",
    "textId": "lastName",
    "label": "Last Name",
    "type": "text"
  },
  {
    "objectId": "input",
    "textId": "maritalStatus",
    "label": "Married",
    "type": "radio",
    "value": "married"
  },
  {
    "objectId": "input",
    "textId": "maritalStatus",
    "label": "Un-Married",
    "type": "radio",
    "value": "un-married"
  },
  {
    "objectId": "input",
    "textId": "language",
    "label": "React",
    "type": "checkbox",
    "value": "react"
  },
  {
    "objectId": "button",
    "id": "1",
    "displayText": "Submit"
  }
]

  export interface Employee{
    firstName: string
    lastName : string
  }


export const createEmptyEmployee = (): Employee => ({
  firstName: "",
  lastName: ""
});